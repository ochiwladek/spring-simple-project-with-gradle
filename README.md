# Requirements

* Java 1.8
    * please set JAVA_HOME environment variable 
* Maven
* Bower
    * Node
    * NPM
# Database

1.Create 2 dbs `charity_market` and `charity_market_test`

# How to run

## Console

1. `bower install`
1. `mvn spring-boot:run`

## IntelliJ
 
1. import this project into IntelliJ by select build.gradle or the folder that is containing  build.gradle  file.
1. `bower install`
1. run Application that has main method.

## Packaging

1. `mvn package`
1. `java -jar target/Charity_Market-1.0-SNAPSHOT.war`
 