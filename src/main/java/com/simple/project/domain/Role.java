package com.simple.project.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author wladek
 */
@Entity
public class Role extends AbstractModel{
    private String name;
    private boolean active;


}
