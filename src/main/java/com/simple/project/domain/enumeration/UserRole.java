package com.simple.project.domain.enumeration;

/**
 * @author Keeun Baik
 */
public enum UserRole {

    ADMIN, USER;

}
