package com.simple.project.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author wladek Baik
 */
@Entity
public class ProjectUser extends AbstractModel{
    @ManyToOne
    private User user;

    @ManyToOne
    private Project project;

    @ManyToOne
    private Role role;

}
