package com.simple.project.service;

import com.simple.project.domain.User;

/**
 * @author wladek
 */
public interface UserService {

    User addNewUser(User user);

    void login(User user);
}
